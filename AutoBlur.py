from PIL import Image
import sys
from guiV1 import Ui_main_window
from PyQt5 import QtCore, QtGui, QtWidgets
import os
import tkinter as tk
from tkinter import filedialog
from blurFunctions import blurFunctions


class gui(Ui_main_window):

    def __init__(self):
        self.DISPLAY_MAX_WIDTH = 1280
        self.DISPLAY_MAX_HEIGHT = 720
        self.file = ''
        self.image_title = ''
        self.save_file = ''
        self.edited_image = Image.new(mode='RGBA', size=(1, 1))
        self.blur_functions = blurFunctions()
        self.probability_threshold = 0.2
        self.face_list = []
        self.face_count = 0
        self.image_open = Image.new(mode='RGBA', size=(1, 1))


    def initFaceList(self):
        self.face_list = []
        self.face_count = 0
        for i in reversed(range(self.scroll_layout.count())):
            self.scroll_layout.itemAt(i).widget().setParent(None)

    def changeImageTitle(self):
        self.image_title = self.input_title.text()

    def browseImageFile(self):
        root = tk.Tk()
        root.withdraw()
        file_path = filedialog.askopenfilename()
        self.input_image_file.setText(file_path)

    def changeImageFile(self):
        self.file = self.input_image_file.text()

    def browseImageSavefile(self):
        root = tk.Tk()
        root.withdraw()
        file_path = filedialog.askdirectory(parent=root)
        self.input_image_savefile.setText(file_path)

    def changeImageSavefile(self):
        self.save_file = self.input_image_savefile.text()

    def saveImage(self):
        if self.edited_image.size != (1, 1):
            if self.isValidDir(self.save_file):
                save_path = self.save_file
                if save_path[len(save_path)-1] != '\\':
                    save_path += '\\'
                if self.image_title != '':
                    save_path += self.image_title
                else:
                    save_path += 'Untitled'
                self.edited_image.save(save_path+'.png', 'png')
            else:
                self.showPathError('Couldn\'t find specified save directory')
        else:
            self.showImageError()

    def isValidPath(self, file_path):
        try:
            test_image = Image.open(file_path)
            test_image = None
            return True
        except:
            return False

    def isValidDir(self, dir_path):
        return os.path.isdir(dir_path)

    def showImage(self, target_image):
        # 60, 90, 1280, 720
        width, height = target_image.size
        image_data = target_image.tobytes('raw', 'RGBA')
        q_image = QtGui.QImage(image_data, target_image.size[0], target_image.size[1], QtGui.QImage.Format_RGBA8888)
        pixmap = QtGui.QPixmap.fromImage(q_image)
        self.label_image.setPixmap(pixmap)
        self.label_image.setGeometry(QtCore.QRect(60+(self.DISPLAY_MAX_WIDTH-width)/2, 90+(self.DISPLAY_MAX_HEIGHT-height)/2, width, height))

    def resizeToFit(self, target_image):
        width, height = target_image.size
        if width > self.DISPLAY_MAX_WIDTH or height > self.DISPLAY_MAX_HEIGHT:
            multiply_ratio = min(self.DISPLAY_MAX_WIDTH/width, self.DISPLAY_MAX_HEIGHT/height)
            new_width = int(width * multiply_ratio)
            new_height = int(height * multiply_ratio)
            target_image = target_image.resize((new_width, new_height))
        return target_image

    def showPathError(self, message):
        error_message = QtWidgets.QMessageBox()
        error_message.setIcon(3)
        error_message.setText(message)
        error_message.setWindowTitle('Error')
        error_message.exec_()

    def showImageError(self):
        error_message = QtWidgets.QMessageBox()
        error_message.setIcon(3)
        error_message.setText('No image to save')
        error_message.setWindowTitle('Error')
        error_message.exec_()

    def toggleFaceBlur(self, face_number):
        check_state = self.face_list[face_number][0]
        start_x = self.face_list[face_number][1]
        start_y = self.face_list[face_number][2]
        width = self.face_list[face_number][3]
        height = self.face_list[face_number][4]
        if not check_state.isChecked():
            new_face = self.blur_functions.unblurFace(self.image_open, start_x, start_y, width, height)
            self.toggle_blur_all.setChecked(False)
        else:
            new_face = self.blur_functions.blurFace(self.image_open, start_x, start_y, width, height)
        self.edited_image.paste(new_face, (start_x, start_y), new_face)
        image_to_show = self.edited_image.copy()
        self.showImage(self.resizeToFit(image_to_show))

    def toggleBlurAll(self):
        if self.toggle_blur_all.isChecked():
            for i in range(self.face_count):
                self.face_list[i][0].setChecked(True)

    def addFaceInList(self, start_x, start_y, width, height):
        check_box = QtWidgets.QCheckBox(self.box_face_toggle)
        check_box.setObjectName('Face'+str(self.face_count))
        check_box.setText('Face '+str(self.face_count))
        check_box.setChecked(True)
        face_nr = int(check_box.text()[5:])
        check_box.stateChanged.connect(lambda: self.toggleFaceBlur(face_nr))
        self.face_list.append([check_box, start_x, start_y, width, height])
        self.face_count += 1
        self.scroll_layout.addWidget(check_box)

    def blurImageInit(self):
        if self.isValidPath(self.file):
            self.initFaceList()

            if self.check_prediction.isChecked():
                result = self.blur_functions.getLocalPrediction(self.file)
            else:
                result = self.blur_functions.getPrediction(self.file)
            result = sorted(result, key=lambda k: k['boundingBox']['left'], reverse=False)
            self.image_open = Image.open(self.file)
            image = self.image_open
            number_of_faces = len(result)
            image_width, image_height = image.size
            blurred_image = Image.new("RGBA", size=image.size, color=(0, 0, 0, 0))
            blurred_image.paste(image, (0, 0))
            for face_number in range(0, number_of_faces):
                if result[face_number]['probability'] >= self.probability_threshold:
                    face_start_x = int(result[face_number]['boundingBox']['left'] * image_width)
                    face_start_y = int(result[face_number]['boundingBox']['top'] * image_height)
                    face_width = int(result[face_number]['boundingBox']['width'] * image_width)
                    face_height = int(result[face_number]['boundingBox']['height'] * image_height)
                    self.addFaceInList(face_start_x, face_start_y, face_width, face_height)
                    face_blurred = self.blur_functions.blurFace(self.image_open, face_start_x, face_start_y, face_width, face_height)
                    blurred_image.paste(face_blurred, (face_start_x, face_start_y), face_blurred)
            self.toggle_blur_all.setChecked(True)
            self.edited_image = blurred_image
            image_to_show = blurred_image.copy()
            self.showImage(self.resizeToFit(image_to_show))
        else:
            self.showPathError('Couldn\'t open specified image')


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    main_window = QtWidgets.QMainWindow()
    ui = gui()
    ui.setupUi(main_window)
    main_window.show()
    sys.exit(app.exec_())
