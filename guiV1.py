# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'MainWindow.ui'
#
# Created by: PyQt5 UI code generator 5.9.2


from PyQt5 import QtCore, QtGui, QtWidgets
import sys
import os


def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)


class Ui_main_window(object):
    def setupUi(self, main_window):
        main_window.setObjectName("main_window")
        main_window.resize(1600, 915)
        font = QtGui.QFont()
        font.setPointSize(12)
        main_window.setFont(font)
        main_window.setTabShape(QtWidgets.QTabWidget.Rounded)
        self.centralwidget = QtWidgets.QWidget(main_window)
        self.centralwidget.setObjectName("centralwidget")
        self.top_bar = QtWidgets.QGroupBox(self.centralwidget)
        self.top_bar.setGeometry(QtCore.QRect(0, 0, 1401, 60))
        self.top_bar.setTitle("")
        self.top_bar.setObjectName("top_bar")
        self.title = QtWidgets.QLabel(self.top_bar)
        self.title.setGeometry(QtCore.QRect(20, -5, 241, 60))
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(0, 170, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 170, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 170, 255, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 170, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 170, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 170, 255, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(120, 120, 120))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(120, 120, 120))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.PlaceholderText, brush)
        self.title.setPalette(palette)
        font = QtGui.QFont()
        font.setPointSize(32)
        font.setBold(True)
        font.setItalic(False)
        font.setWeight(75)
        font.setKerning(True)
        self.title.setFont(font)
        self.title.setObjectName("title")
        self.text_select_image = QtWidgets.QLabel(self.top_bar)
        self.text_select_image.setGeometry(QtCore.QRect(290, 10, 351, 40))
        font = QtGui.QFont()
        font.setPointSize(20)
        self.text_select_image.setFont(font)
        self.text_select_image.setObjectName("text_select_image")
        self.button_browse_file = QtWidgets.QPushButton(self.top_bar)
        self.button_browse_file.setGeometry(QtCore.QRect(1054, 14, 91, 32))
        font = QtGui.QFont()
        font.setKerning(True)
        self.button_browse_file.setIcon(QtGui.QIcon(resource_path('file_browser_button.png')))
        self.button_browse_file.setIconSize(QtCore.QSize(89, 30))
        self.button_browse_file.setObjectName("button_browse_file")
        font = QtGui.QFont()
        font.setPointSize(8)
        self.button_blur = QtWidgets.QPushButton(self.top_bar)
        self.button_blur.setGeometry(QtCore.QRect(1200, 10, 91, 40))
        font = QtGui.QFont()
        font.setPointSize(16)
        self.button_blur.setFont(font)
        self.button_blur.setObjectName("button_blur")
        self.button_browse_file.raise_()
        self.title.raise_()
        self.text_select_image.raise_()
        self.button_blur.raise_()
        self.bottom_bar = QtWidgets.QGroupBox(self.centralwidget)
        self.bottom_bar.setGeometry(QtCore.QRect(0, 840, 1401, 60))
        self.bottom_bar.setTitle("")
        self.bottom_bar.setObjectName("bottom_bar")
        self.text_save_folder = QtWidgets.QLabel(self.bottom_bar)
        self.text_save_folder.setGeometry(QtCore.QRect(50, 15, 181, 30))
        font = QtGui.QFont()
        font.setPointSize(20)
        self.text_save_folder.setFont(font)
        self.text_save_folder.setObjectName("text_save_folder")
        self.input_image_savefile = QtWidgets.QLineEdit(self.bottom_bar)
        self.input_image_savefile.setGeometry(QtCore.QRect(245, 15, 401, 30))
        self.input_image_savefile.setObjectName("input_image_savefile")
        self.label = QtWidgets.QLabel(self.bottom_bar)
        self.label.setGeometry(QtCore.QRect(770, 15, 71, 30))
        font = QtGui.QFont()
        font.setPointSize(20)
        self.label.setFont(font)
        self.label.setObjectName("label")
        font = QtGui.QFont()
        font.setPointSize(8)
        self.button_browse_savefile = QtWidgets.QPushButton(self.bottom_bar)
        self.button_browse_savefile.setGeometry(QtCore.QRect(644, 14, 91, 32))
        font = QtGui.QFont()
        font.setKerning(True)
        self.button_browse_savefile.setIcon(QtGui.QIcon(resource_path('file_browser_button.png')))
        self.button_browse_savefile.setIconSize(QtCore.QSize(89, 30))
        self.button_browse_savefile.setObjectName("button_browse_savefile")
        self.input_title = QtWidgets.QLineEdit(self.bottom_bar)
        self.input_title.setGeometry(QtCore.QRect(865, 15, 201, 30))
        self.input_title.setObjectName("input_title")
        self.button_save = QtWidgets.QPushButton(self.bottom_bar)
        self.button_save.setGeometry(QtCore.QRect(1125, 10, 91, 40))
        font = QtGui.QFont()
        font.setPointSize(16)
        self.button_save.setFont(font)
        self.button_save.setObjectName("button_save")
        self.check_prediction = QtWidgets.QCheckBox(self.bottom_bar)
        self.check_prediction.setGeometry(QtCore.QRect(1270, 15, 120, 30))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.check_prediction.setFont(font)
        self.check_prediction.setObjectName('check_prediction')
        self.check_prediction.setText('Local Pred.')
        self.button_browse_savefile.raise_()
        self.text_save_folder.raise_()
        self.input_image_savefile.raise_()
        self.label.raise_()
        self.input_title.raise_()
        self.button_save.raise_()
        self.check_prediction.raise_()
        self.input_image_file = QtWidgets.QLineEdit(self.centralwidget)
        self.input_image_file.setGeometry(QtCore.QRect(655, 15, 401, 30))
        self.input_image_file.setObjectName("input_image_file")
        self.side_bar = QtWidgets.QGroupBox(self.centralwidget)
        self.side_bar.setGeometry(QtCore.QRect(1400, 0, 200, 900))
        self.side_bar.setTitle("")
        self.side_bar.setObjectName("side_bar")
        self.side_bar_title = QtWidgets.QGroupBox(self.side_bar)
        self.side_bar_title.setGeometry(QtCore.QRect(0, 0, 200, 60))
        self.side_bar_title.setTitle("")
        self.side_bar_title.setObjectName("side_bar_title")
        self.bar_title = QtWidgets.QLabel(self.side_bar_title)
        self.bar_title.setGeometry(QtCore.QRect(20, 0, 161, 60))
        font = QtGui.QFont()
        font.setPointSize(24)
        self.bar_title.setFont(font)
        self.bar_title.setObjectName("bar_title")

        self.box_face_toggle = QtWidgets.QWidget(self.side_bar)
        self.box_face_toggle.setGeometry(QtCore.QRect(35, 110, 131, 771))
        self.box_face_toggle.setObjectName("box_face_toggle")
        self.scroll_area = QtWidgets.QScrollArea(self.box_face_toggle)
        self.scroll_area.setFixedWidth(131)
        self.scroll_area.setFixedHeight(771)
        self.scroll_area.setWidgetResizable(True)
        self.scroll_widget = QtWidgets.QWidget()
        self.scroll_area.setWidget(self.scroll_widget)
        self.scroll_layout = QtWidgets.QVBoxLayout(self.scroll_widget)

        self.toggle_blur_all = QtWidgets.QCheckBox(self.side_bar)
        self.toggle_blur_all.setGeometry(QtCore.QRect(55, 70, 91, 30))
        self.toggle_blur_all.setObjectName("toggle_blur_all")
        self.label_image = QtWidgets.QLabel(self.centralwidget)
        self.label_image.setGeometry(QtCore.QRect(60, 90, 1280, 720))
        self.label_image.setObjectName("label_image")
        main_window.setCentralWidget(self.centralwidget)

        self.input_title.textChanged.connect(self.changeImageTitle)
        self.button_browse_file.clicked.connect(self.browseImageFile)
        self.input_image_file.textChanged.connect(self.changeImageFile)
        self.button_browse_savefile.clicked.connect(self.browseImageSavefile)
        self.input_image_savefile.textChanged.connect(self.changeImageSavefile)
        self.button_save.clicked.connect(self.saveImage)
        self.button_blur.clicked.connect(self.blurImageInit)
        self.toggle_blur_all.stateChanged.connect(self.toggleBlurAll)

        self.retranslateUi(main_window)
        QtCore.QMetaObject.connectSlotsByName(main_window)


    def retranslateUi(self, main_window):
        _translate = QtCore.QCoreApplication.translate
        main_window.setWindowTitle(_translate("main_window", "AutoBlur"))
        self.title.setText(_translate("main_window", "AutoBlur"))
        self.text_select_image.setText(_translate("main_window", "Select an image to blur:"))
        self.button_blur.setText(_translate("main_window", "Blur"))
        self.text_save_folder.setText(_translate("main_window", "Save Folder:"))
        self.label.setText(_translate("main_window", "Title:"))
        self.button_save.setText(_translate("main_window", "Save"))
        self.bar_title.setText(_translate("main_window", "Face List"))
        self.toggle_blur_all.setText(_translate("main_window", "Blur All"))
        self.label_image.setText(_translate("main_window", ""))


    def changeImageTitle(self):
        pass

    def browseImageFile(self):
        pass

    def changeImageFile(self):
        pass

    def browseImageSavefile(self):
        pass

    def changeImageSavefile(self):
        pass

    def saveImage(self):
        pass

    def showImage(self, target_image):
        pass

    def blurImageInit(self):
        pass

    def addCheck(self):
        pass

    def toggleBlurAll(self):
        pass


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    main_window = QtWidgets.QMainWindow()
    ui = Ui_main_window()
    ui.setupUi(main_window)
    main_window.show()
    sys.exit(app.exec_())

