# -*- mode: python ; coding: utf-8 -*-

block_cipher = None

added_files = [('file_browser_button.png', '.'), ('labels.txt', '.')]
added_binaries = [('model.pb', '.')] 
a = Analysis(['autoblur.py'],
             pathex=['D:\\anaconda\\envs\\autoblur\\Lib\\site-packages\\PyQt5\\Qt\\bin', 'd:\\autoblur'],
             binaries=added_binaries,
             datas=added_files,
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
#a.datas += [('file_browser_button.png', 'D:\\autoblur\\file_browser_button.png', 'DATA')]
#a.binaries += [('model.pb', '.', 'BINARY')]
#a.datas += [('labels.txt', '.', 'DATA')]
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='autoblur',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=False,
          icon='D:\\autoblur\\abicon.ico')