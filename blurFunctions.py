from PIL import Image, ImageFilter, ImageDraw
import tensorflow as tf
from predict import TFObjectDetection
import requests
import io
import os
import math
import numpy as np
import sys


def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)


class blurFunctions:
    def blurFace(self, image, start_x, start_y, width, height):
        image = image.convert('RGB')
        face = image.crop((start_x, start_y, start_x + width, start_y + height))
        face = face.filter(ImageFilter.GaussianBlur(12))
        np_face = np.array(face)
        alpha = Image.new('L', (width, height), 0)
        draw = ImageDraw.Draw(alpha)
        draw.pieslice([0, 0, width, height], 0, 360, fill=255)
        np_alpha = np.array(alpha)
        np_face = np.dstack((np_face, np_alpha))
        oval_face = Image.fromarray(np_face)
        return oval_face

    def unblurFace(self, image, start_x, start_y, width, height):
        face = image.crop((start_x, start_y, start_x + width, start_y + height))
        face = face.convert('RGBA')
        return face

    def round_down(self, n, decimals=0):
        multiplier = 10 ** decimals
        return math.floor(n * multiplier) / multiplier

    def prepareForPrediction(self, target_file):
        image_info = os.stat(target_file)
        image_size = image_info.st_size
        max_size = 4194304
        if image_size > max_size:
            multiply_ratio = self.round_down(max_size / image_size, 2)
            pil_image = Image.open(target_file)
            suffix = pil_image.format
            width, height = pil_image.size
            pil_image = pil_image.resize((int(width * multiply_ratio), int(height * multiply_ratio)))
            byte_image = io.BytesIO()
            pil_image.save(byte_image, suffix)
            return byte_image.getvalue()
        else:
            return open(target_file, 'rb').read()

    def prepareForLocalPrediction(self, target_file):
        image_info = os.stat(target_file)
        image_size = image_info.st_size
        pil_image = Image.open(target_file)
        max_size = 4194304
        if image_size > max_size:
            multiply_ratio = self.round_down(max_size / image_size, 1)
            width, height = pil_image.size
            pil_image = pil_image.resize((int(width * multiply_ratio), int(height * multiply_ratio)))
        return pil_image

    def getPrediction(self, target_file):
        cv_headers = {'Prediction-Key': "f886aff2872b4b0fb235f521ee7b7168", 'Content-Type': "application/json"}

        image_send = self.prepareForPrediction(target_file)
        URL = "https://westeurope.api.cognitive.microsoft.com/customvision/v3.0/Prediction/49447b76-2076-403e-b59d-76b9477fd342/detect/iterations/Iteration11/image"
        cv_response = requests.post(URL, headers=cv_headers, data=image_send)
        cv_response.raise_for_status()
        prediction = cv_response.json()
        return prediction['predictions']

    def getLocalPrediction(self, target_file):
        MODEL_FILENAME = 'model.pb'
        LABELS_FILENAME = 'labels.txt'

        model_path = resource_path(MODEL_FILENAME)
        labels_path = resource_path(LABELS_FILENAME)

        graph_def = tf.compat.v1.GraphDef()
        with tf.io.gfile.GFile(model_path, 'rb') as f:
            graph_def.ParseFromString(f.read())

        with open(labels_path, 'r') as f:
            labels = [l.strip() for l in f.readlines()]

        od_model = TFObjectDetection(graph_def, labels)

        img = self.prepareForLocalPrediction(target_file)
        predictions = od_model.predict_image(img)
        return predictions
