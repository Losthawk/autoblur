Application uses customvision detection model
Non-local prediction may not be available after Azure trial expires


Tutorial:
-select an image using the file browser (top bar)

-click the blur button (top bar)

-the image should show up with all the faces blurred (center)

-a checkbox should show up for each face (side bar)

-each checkbox toggles the blur o a face, ordered from left to right ("face 0" is left-most)

-toggling off the "blur all" checkbox does nothing, but toggling it on automatically re-blurs all the faces

-select the folder in which the image should be saved WITHOUT writing the image title in the file browser (bottom bar)

-write the title of the image in the title box (bottom bar)

-after completing all the boxes and after editing the image, click the "save" button to save the edited image in the specified folder with the specified title (bottom bar)